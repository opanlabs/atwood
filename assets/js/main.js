$(window).scroll(function () {
        var sc = $(window).scrollTop()
        if (sc > 50) {
            $("#header").addClass("white")
        } else {
            $("#header").removeClass("white")
        }
    });

$( document ).ready(function() {
    
	$('.slider-bg').slick({
		fade: true,
		dots: true,
		arrows: false
	});

    $('.datepicker').datepicker();

  $('.content-column-slider').slick({
    fade: true,
    dots: false,
    nextArrow: '<i class="fas fa-angle-right next"></i>',
    prevArrow: '<i class="fas fa-angle-left prev"></i>',
  });


	$('.img-slider').slick({
    infinite: false,
    fade: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.content-slider',
    prevArrow: $(".prevx"),
    nextArrow: $(".nextx")
    // nextArrow: '<button type="button" class="slick-next">></button>',
    // prevArrow: '<button type="button" class="slick-prev"><</button>'
  });
  $('.content-slider').slick({
    infinite: false,
    fade: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.img-slider',
    prevArrow: $(".prevx"),
    nextArrow: $(".nextx")
    // nextArrow: '<button type="button" class="slick-next">></button>',
    // prevArrow: '<button type="button" class="slick-prev"><</button>'
  });

	$("[data-fancybox='kitchen']").fancybox({
		// Options will go here
	});

});